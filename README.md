# URL Lengthener
 
Ever look at a shortened URL and wonder what it is linking to? Don't trust the person sending you the URL? Use URL Lengthener to find out what the real link is! Supports all major URL shortener services. The definitive URL Lengthener on Windows 8!

URL Lengthener is a Windows 8 Modern app, available on the Windows Store.