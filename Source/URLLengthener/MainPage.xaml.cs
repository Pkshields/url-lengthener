﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.Storage;
using Windows.UI.ApplicationSettings;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace URLLengthener
{
    public struct CacheData
    {
        /// <summary>
        /// Short URL
        /// </summary>
        public string ShortUrl;
        
        /// <summary>
        /// Long URL
        /// </summary>
        public string LongUrl;

        /// <summary>
        /// Uri for the Url
        /// </summary>
        public Uri LongUrlUri;

        /// <summary>
        /// Initialize this thing.
        /// </summary>
        /// <param name="shortUrl">Short URL</param>
        /// <param name="longUrl">Long URL</param>
        public CacheData(string shortUrl, string longUrl)
        {
            ShortUrl = shortUrl;
            LongUrl = longUrl;
            LongUrlUri = new Uri(LongUrl);
        }
    }

    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : URLLengthener.Common.LayoutAwarePage
    {
        /// <summary>
        /// access to the LongURL database
        /// </summary>
        private LongURL url;

        /// <summary>
        /// Holder of all the cacheData
        /// </summary>
        private CacheData[] cacheData;

        /// <summary>
        /// Number of cache entries in the cache
        /// </summary>
        private int cacheDataNum;

        /// <summary>
        /// URL to the privacy policy
        /// </summary>
        private const string privacyPolicyUrl = "http://muzene.com/win8apps/privacypolicy/#urllengthener";

        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            //If we are not at first start
            if (pageState != null)
            {
                //Check if there is a value for LongURL in the pageState
                object obj;
                if (pageState.TryGetValue("LongURL", out obj))
                    url = (LongURL)obj;
                else
                    url = new LongURL();
            }
            else
                //Create a new LongURL access point
                url = new LongURL();

            //Get the CachaData from the local storage (if available)
            ApplicationDataContainer localStorage = ApplicationData.Current.LocalSettings;
            if (localStorage.Values.ContainsKey("cacheData") && localStorage.Values.ContainsKey("cacheDataNum"))
            {
                cacheData = (CacheData[])localStorage.Values["cacheData"];
                cacheDataNum = (int)localStorage.Values["cacheDataNum"];

                //Populate the cache with this data
                PopulateCache();
            }
            else
            {
                //Initialize the CacheData array with a new, blank array if nothing else available
                cacheData = new CacheData[5];
                cacheDataNum = 0;
            }

            //Add the privacy policy to the settings pane
            SettingsPane.GetForCurrentView().CommandsRequested += MainPage_CommandsRequested;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            //Store stuff for access later
            pageState.Add("LongURL", url);

            //Store the cache data in the local storage
            ApplicationDataContainer localStorage = ApplicationData.Current.LocalSettings;
            localStorage.Values["cacheData"] = cacheData;
            localStorage.Values["cacheDataNum"] = cacheDataNum;
        }

        /// <summary>
        /// Populate the correct places for a correct URL
        /// </summary>
        /// <param name="shortUrl">Short URL</param>
        /// <param name="longUrl">Long URL</param>
        public void SuccessfulURL(string shortUrl, string longUrl)
        {
            //Stick the LongURL in the textbox
            LongBox.Text = longUrl;

            //If there are less than 5 items in the data cache, just add it to the end of the list
            if (cacheDataNum < 5)
            {
                for (int i = cacheDataNum - 1; i >= 0; i--)
                    cacheData[i + 1] = cacheData[i];

                cacheData[0] = new CacheData(shortUrl, longUrl);
                cacheDataNum++;
            }
            //Else, move everything up one and add the new data to the end
            else
            {
                for (int i = 3; i >= 0; i--)
                    cacheData[i + 1] = cacheData[i];

                cacheData[0] = new CacheData(shortUrl, longUrl);
            }

            //Poplate the cache UI
            PopulateCache();
        }

        /// <summary>
        /// Poplate the cache UI
        /// </summary>
        private void PopulateCache()
        {
            //Add the data to the UI through hella ifs
            if (cacheDataNum >= 1)
            {
                CacheShort1Text.Text = cacheData[0].ShortUrl;
                CacheLong1Text.Content = cacheData[0].LongUrl;
                CacheLong1Text.NavigateUri = cacheData[0].LongUrlUri;
                CacheShort1Text.Visibility = Visibility.Visible;
                CacheLong1Text.Visibility = Visibility.Visible;
            }

            if (cacheDataNum >= 2)
            {
                CacheShort2Text.Text = cacheData[1].ShortUrl;
                CacheLong2Text.Content = cacheData[1].LongUrl;
                CacheLong2Text.NavigateUri = cacheData[1].LongUrlUri;
                CacheShort2Text.Visibility = Visibility.Visible;
                CacheLong2Text.Visibility = Visibility.Visible;
            }

            if (cacheDataNum >= 3)
            {
                CacheShort3Text.Text = cacheData[2].ShortUrl;
                CacheLong3Text.Content = cacheData[2].LongUrl;
                CacheLong3Text.NavigateUri = cacheData[2].LongUrlUri;
                CacheShort3Text.Visibility = Visibility.Visible;
                CacheLong3Text.Visibility = Visibility.Visible;
            }

            if (cacheDataNum >= 4)
            {
                CacheShort4Text.Text = cacheData[3].ShortUrl;
                CacheLong4Text.Content = cacheData[3].LongUrl;
                CacheLong4Text.NavigateUri = cacheData[3].LongUrlUri;
                CacheShort4Text.Visibility = Visibility.Visible;
                CacheLong4Text.Visibility = Visibility.Visible;
            }

            if (cacheDataNum >= 5)
            {
                CacheShort5Text.Text = cacheData[4].ShortUrl;
                CacheLong5Text.Content = cacheData[4].LongUrl;
                CacheLong5Text.NavigateUri = cacheData[4].LongUrlUri;
                CacheShort5Text.Visibility = Visibility.Visible;
                CacheLong5Text.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Lengthen the entered URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void LengthenButton_Click(object sender, RoutedEventArgs e)
        {
            //Set the app to loading
            LoadingBar.Visibility = Visibility.Visible;

            //Get the data from the ShortURL box
            string shortUrl = ShortBox.Text;

            //Try to get a shorter URL back from the database
            LongUrlError result;
            try
            {
                result = await url.GetData(shortUrl);
            }
            catch
            {
                result = LongUrlError.UrlError;
            }

            //Check the returned error
            switch(result)
            {
                case LongUrlError.Success:
                    SuccessfulURL(shortUrl, url.Data);
                    ErrorBox.Visibility = Visibility.Collapsed;
                    break;

                case LongUrlError.NoInternet:
                    ErrorBox.Text = "No internet connection available.";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
                
                case LongUrlError.UnsupportedUrl:
                    ErrorBox.Text = "URL entered is not supported by this service.";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
                
                case LongUrlError.UrlError:
                    ErrorBox.Text = "The website contained within this shortened URL has returned an error.";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
            }

            //Set the app to not loading anymore
            LoadingBar.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Add commands to the settings pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void MainPage_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            //Clear out the current ApplicationCommands
            args.Request.ApplicationCommands.Clear();

            //Create the Privacy Policy command
            SettingsCommand privacyPref = new SettingsCommand("privacyPref", "Privacy Policy", (uiCommand) =>
            {
                LaunchPrivacyPolicy();
            });

            //Add the Privacy Policy command to the Settings pane
            args.Request.ApplicationCommands.Add(privacyPref);
        }

        /// <summary>
        /// Launch the URL to the privacy policy link
        /// </summary>
        private async void LaunchPrivacyPolicy()
        {
            //Create the link to the privacy policy and launch it
            Uri privacyLink = new Uri(privacyPolicyUrl);
            await Windows.System.Launcher.LaunchUriAsync(privacyLink);
        }
    }
}
