﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using System.Net;
using Windows.Data.Xml.Dom;
using System.Diagnostics;

namespace URLLengthener
{
    enum LongUrlError
    {
        NoInternet,
        UnsupportedUrl,
        UrlError,
        Success
    }

    class LongURL
    {
        /// <summary>
        /// List of supported shortening sites
        /// </summary>
        private string[] supportedList;

        /// <summary>
        /// Data returned from the database
        /// </summary>
        public string Data
        {
            get;
            private set;
        }

        /// <summary>
        /// Part 1 of the database link
        /// </summary>
        public const string dbAccessUrlPart1 = "http://api.longurl.org/v2/expand?url=";

        /// <summary>
        /// Part 2 of the database link
        /// </summary>
        public const string dbAccessUrlPart2 = "&response-code=1";

        /// <summary>
        /// Create an instance of LongURL
        /// </summary>
        public LongURL()
        {
            //Initialize with default data
            Data = "";

            //Get list of supported URLs from the other class
            supportedList = LongURLSupportedList.GetSupportedList();
        }

        /// <summary>
        /// Get a longer URL from the short URL
        /// </summary>
        /// <param name="url">URL to lengthen</param>
        /// <returns>Error code</returns>
        public async Task<LongUrlError> GetData(string url)
        {
            //Check if we have an internet connection
            bool result = IsInternet();
            if (!result)
                return LongUrlError.NoInternet;

            //Parse the URL, checking if it is supported at the same time
            result = ParseURL(ref url);
            if (!result)
                return LongUrlError.UnsupportedUrl;

            //Concatenate the URL to access the database
            Uri resultUri = new Uri(dbAccessUrlPart1 + url + dbAccessUrlPart2);

            //Access the database
            XmlDocument xmlDocument = await XmlDocument.LoadFromUriAsync(resultUri);
            IXmlNode responseNode = xmlDocument.SelectSingleNode("response");

            //Check the response node, if it isn anything other than 200 then there is a problem with the website itself, or the URL 
            //is pointing at a bunk link
            IXmlNode codeNode = responseNode.SelectSingleNode("response-code");
            if (codeNode.InnerText != "200")
                return LongUrlError.UrlError;

            //Great! Get the full URL, store it and send back success
            IXmlNode addressNode = responseNode.SelectSingleNode("long-url");
            Data = addressNode.InnerText;

            return LongUrlError.Success;
        }

        /// <summary>
        /// Parse the URL into a valid HTTP , encoded link
        /// </summary>
        /// <param name="url">URL string to parse</param>
        private bool ParseURL(ref string url)
        {
            //Check if the website is supported by the service
            bool result = false;
            foreach (string possibleUrl in supportedList)
            {
                result = url.Contains(possibleUrl);
                if (result)
                    break;
            }

            //If it isn't return false
            if (!result)
                return false;

            //Parse the URL into a valid URI
            UriBuilder parsedUrl = new UriBuilder(url);
            Uri urlLocal = parsedUrl.Uri;

            //Check if the URL is a valid HTTP(S) URL
            result = (urlLocal.Scheme == "http" || urlLocal.Scheme == "https");

            //If it isn't return false
            if (!result)
                return false;

            //Encode the URL for the API
            url = WebUtility.HtmlEncode(urlLocal.AbsoluteUri);

            return true;
        }

        /// <summary>
        /// Check if we have access to the internet
        /// </summary>
        /// <returns>If we have internet connection</returns>
        public static bool IsInternet()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }
    }
}
